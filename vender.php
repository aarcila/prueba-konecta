<?php
require_once "conexion.php";

$cantidad = "";
$cantidad_err = "";

if (isset($_POST["id"]) && !empty($_POST["id"])) {
    $id = $_POST["id"];
    $input_cantidad = trim($_POST["cantidad"]);

    if (empty($input_cantidad)) {
        $cantidad_err = "Por favor ingresa un cantidad.";
    } else {
        $cantidad = $input_cantidad;
    }
    $sql = "SELECT * FROM producto WHERE id = ?";
    if ($stmt = mysqli_prepare($link, $sql)) {
        mysqli_stmt_bind_param($stmt, "i", $param_id);

        $param_id = $id;

        if (mysqli_stmt_execute($stmt)) {
            $result = mysqli_stmt_get_result($stmt);

            if (mysqli_num_rows($result) == 1) {
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            } else {
                header("location: error.php");
                exit();
            }
        } else {
            echo "Algo fue mal, intenta de nuevo.";
        }
    }
    if (empty($cantidad_err)) {
        if ($row['stock'] - $cantidad > 0) {
            $sql = "INSERT INTO `venta` (`cantidad`, `producto_id`) VALUES (?, ?)";

            if ($stmt = mysqli_prepare($link, $sql)) {
                mysqli_stmt_bind_param($stmt, "ss", $param_cantidad, $param_id);

                $param_cantidad = $cantidad;
                $param_id = $id;

                if (mysqli_stmt_execute($stmt)) {
                    $stockTotal = $row['stock'] - $cantidad;
                    $query = "UPDATE producto SET stock=? WHERE id = ?";
                    $sqlQuery = mysqli_prepare($link, $query);
                    mysqli_stmt_bind_param($sqlQuery, "si", $param_stock, $param_id);
                    $param_stock = $stockTotal;
                    $param_id = $id;
                    if (mysqli_stmt_execute($sqlQuery)) {
                        header("location: index.php");
                        exit();
                    }
                } else {
                    echo "Algo fue mal, intenta de nuevo.";
                }
            }

            mysqli_stmt_close($stmt);
        } else {
            echo "No hay inventario suficiente.";
        }
    }

    mysqli_close($link);
} else {
    if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
        $id =  trim($_GET["id"]);

        $sql = "SELECT * FROM producto WHERE id = ?";
        if ($stmt = mysqli_prepare($link, $sql)) {
            mysqli_stmt_bind_param($stmt, "i", $param_id);

            $param_id = $id;

            if (mysqli_stmt_execute($stmt)) {
                $result = mysqli_stmt_get_result($stmt);

                if (mysqli_num_rows($result) == 1) {
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

                    $nombre = $row["nombre"];
                    $referencia = $row["referencia"];
                    $precio = $row["precio"];
                    $peso = $row["peso"];
                    $categoria = $row["categoria"];
                    $stock = $row["stock"];
                    $fecha = $row["fecha_creacion"];
                } else {
                    header("location: error.php");
                    exit();
                }
            } else {
                echo "Algo fue mal, intenta de nuevo.";
            }
        }

        mysqli_stmt_close($stmt);

        mysqli_close($link);
    } else {
        header("location: error.php");
        exit();
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Editar producto</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper {
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mt-5">Vender producto: <?php echo $row['nombre'] ?></h2>
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                        <div class="row">
                            <div class="form-group col-6">
                                <label>Cantidad</label>
                                <input type="number" name="cantidad" class="form-control <?php echo (!empty($cantidad_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $cantidad; ?>">
                                <span class="invalid-feedback"><?php echo $cantidad_err; ?></span>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" />
                        <input type="submit" class="btn btn-primary" value="Vender">
                        <a href="index.php" class="btn btn-secondary ml-2">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>