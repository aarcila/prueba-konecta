<?php
require_once "conexion.php";

$nombre = $referencia = $precio = $peso = $categoria = $stock = $fecha = "";
$nombre_err = $referencia_err = $precio_err = $peso_err = $categoria_err = $stock_err = $fecha_err = "";

if (isset($_POST["id"]) && !empty($_POST["id"])) {
    $id = $_POST["id"];

    $input_nombre = trim($_POST["nombre"]);
    if (empty($input_nombre)) {
        $nombre_err = "Por favor ingresa un nombre.";
    } else {
        $nombre = $input_nombre;
    }

    $input_referencia = trim($_POST["referencia"]);
    if (empty($input_referencia)) {
        $referencia_err = "Por favor ingresa una referencia.";
    } else {
        $referencia = $input_referencia;
    }

    $input_precio = trim($_POST["precio"]);
    if (empty($input_precio)) {
        $precio_err = "Por favor ingresa un precio.";
    } else {
        $precio = $input_precio;
    }

    $input_peso = trim($_POST["peso"]);
    if (empty($input_peso)) {
        $peso_err = "Por favor ingresa un peso.";
    } else {
        $peso = $input_peso;
    }

    $input_categoria = trim($_POST["categoria"]);
    if (empty($input_categoria)) {
        $categoria_err = "Por favor ingresa una categoria.";
    } else {
        $categoria = $input_categoria;
    }

    $input_stock = trim($_POST["stock"]);
    if (empty($input_stock)) {
        $stock_err = "Por favor ingresa un stock.";
    } else {
        $stock = $input_stock;
    }

    $input_fecha = trim($_POST["fecha_creacion"]);
    if (empty($input_fecha)) {
        $fecha_err = "Por favor ingresa un fecha.";
    } else {
        $fecha = $input_fecha;
    }

    if (empty($nombre_err) && empty($referencia_err) && empty($precio_err) && empty($peso_err) && empty($categoria_err) && empty($stock_err) && empty($fecha_err)) {
        $sql = "UPDATE producto SET nombre=?, referencia=?, precio=?, peso=?, categoria=? ,stock=? ,fecha_creacion=?  WHERE id=?";

        if ($stmt = mysqli_prepare($link, $sql)) {
            mysqli_stmt_bind_param($stmt, "ssiisisi", $param_nombre, $param_referencia, $param_precio, $param_peso, $param_categoria, $param_stock, $param_fecha, $param_id);

            $param_nombre = $nombre;
            $param_referencia = $referencia;
            $param_precio = $precio;
            $param_peso = $peso;
            $param_categoria = $categoria;
            $param_stock = $stock;
            $param_fecha = $fecha;
            $param_id = $id;

            if (mysqli_stmt_execute($stmt)) {
                header("location: index.php");
                exit();
            } else {
                echo "Algo fue mal, intenta de nuevo.";
            }
        }

        mysqli_stmt_close($stmt);
    }

    mysqli_close($link);
} else {
    if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
        $id =  trim($_GET["id"]);

        $sql = "SELECT * FROM producto WHERE id = ?";
        if ($stmt = mysqli_prepare($link, $sql)) {
            mysqli_stmt_bind_param($stmt, "i", $param_id);

            $param_id = $id;

            if (mysqli_stmt_execute($stmt)) {
                $result = mysqli_stmt_get_result($stmt);

                if (mysqli_num_rows($result) == 1) {
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

                    $nombre = $row["nombre"];
                    $referencia = $row["referencia"];
                    $precio = $row["precio"];
                    $peso = $row["peso"];
                    $categoria = $row["categoria"];
                    $stock = $row["stock"];
                    $fecha = $row["fecha_creacion"];
                } else {
                    header("location: error.php");
                    exit();
                }
            } else {
                echo "Algo fue mal, intenta de nuevo.";
            }
        }

        mysqli_stmt_close($stmt);

        mysqli_close($link);
    } else {
        header("location: error.php");
        exit();
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Editar producto</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper {
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mt-5">Editar producto</h2>
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                        <div class="row">
                            <div class="form-group col-6">
                                <label>Nombre</label>
                                <input type="text" name="nombre" class="form-control <?php echo (!empty($nombre_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $nombre; ?>">
                                <span class="invalid-feedback"><?php echo $nombre_err; ?></span>
                            </div>
                            <div class="form-group col-6">
                                <label>Referencia</label>
                                <input type="text" name="referencia" class="form-control <?php echo (!empty($referencia_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $referencia; ?>">
                                <span class="invalid-feedback"><?php echo $referencia_err; ?></span>
                            </div>
                            <div class="form-group col-6">
                                <label>Precio</label>
                                <input type="number" name="precio" class="form-control <?php echo (!empty($precio_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $precio; ?>">
                                <span class="invalid-feedback"><?php echo $precio_err; ?></span>
                            </div>
                            <div class="form-group col-6">
                                <label>Peso</label>
                                <input type="number" name="peso" class="form-control <?php echo (!empty($peso_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $peso; ?>">
                                <span class="invalid-feedback"><?php echo $peso_err; ?></span>
                            </div>
                            <div class="form-group col-6">
                                <label>Categoria</label>
                                <input type="text" name="categoria" class="form-control <?php echo (!empty($categoria_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $categoria; ?>">
                                <span class="invalid-feedback"><?php echo $categoria_err; ?></span>
                            </div>
                            <div class="form-group col-6">
                                <label>Stock</label>
                                <input type="number" name="stock" class="form-control <?php echo (!empty($stock_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $stock; ?>">
                                <span class="invalid-feedback"><?php echo $stock_err; ?></span>
                            </div>
                            <div class="form-group col-6">
                                <label>Fecha</label>
                                <input type="date" name="fecha_creacion" class="form-control <?php echo (!empty($fecha_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $fecha; ?>">
                                <span class="invalid-feedback"><?php echo $fecha_err; ?></span>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" />
                        <input type="submit" class="btn btn-primary" value="Editar">
                        <a href="index.php" class="btn btn-secondary ml-2">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>