<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Prueba</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        table tr td:last-child {
            width: 120px;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</head>


<body>
    <div class="wrapper">
        <div class="container-fluid">
            <h2 class="mt-2">Productos</h2>
            <div class="mt-3 mb-3">
                <a href="crear.php" class="btn btn-info">Crear nuevo producto</a>
                <a href="listaVentas.php" class="btn btn-success">Ventas</a>
            </div>
            <?php
            require_once "conexion.php";

            $sql = "SELECT * FROM producto";
            if ($result = mysqli_query($link, $sql)) {
                if (mysqli_num_rows($result) > 0) {
                    echo '<table class="table table-bordered table-responsive-lg table-striped">';
                    echo "<thead>";
                    echo "<tr>";
                    echo "<th>#</th>";
                    echo "<th>Nombre</th>";
                    echo "<th>Referencia</th>";
                    echo "<th>Precio</th>";
                    echo "<th>Peso</th>";
                    echo "<th>Categoría</th>";
                    echo "<th>Stock</th>";
                    echo "<th>Fecha</th>";
                    echo "<th>Acciones</th>";
                    echo "</tr>";
                    echo "</thead>";
                    echo "<tbody>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo "<tr>";
                        echo "<td>" . $row['id'] . "</td>";
                        echo "<td>" . $row['nombre'] . "</td>";
                        echo "<td>" . $row['referencia'] . "</td>";
                        echo "<td>" . $row['precio'] . "</td>";
                        echo "<td>" . $row['peso'] . "</td>";
                        echo "<td>" . $row['categoria'] . "</td>";
                        echo "<td>" . $row['stock'] . "</td>";
                        echo '<td style="width: 500px !important">' . $row['fecha_creacion'] . '</td>';
                        echo "<td>";
                        echo '<a href="editar.php?id=' . $row['id'] . '" class="mr-3" title="Editar" data-toggle="tooltip"><span class="fa fa-pencil"></span></a>';
                        echo '<a href="vender.php?id=' . $row['id'] . '" class="mr-3" title="Vender" data-toggle="tooltip"><span class="fa fa-shopping-cart"></span></a>';
                        echo '<a href="eliminar.php?id=' . $row['id'] . '" title="Eliminar" data-toggle="tooltip"><span class="fa fa-trash"></span></a>';
                        echo "</td>";
                        echo "</tr>";
                    }
                    echo "</tbody>";
                    echo "</table>";
                    mysqli_free_result($result);
                } else {
                    echo '<div class="alert alert-danger"><em>No hay datos.</em></div>';
                }
            } else {
                echo "Algo fue mal, intenta de nuevo.";
            }

            mysqli_close($link);
            ?>
        </div>
    </div>
</body>

</html>